import os
import numpy as np

from keras.preprocessing import image
from keras.utils import to_categorical
from keras import layers
#from keras.models import Sequential
from keras.layers import Dense, Flatten
from keras.layers import Conv2D, MaxPooling2D
from keras.layers import Input
from keras.layers import concatenate
from keras.models import Model
from keras.optimizers import SGD
from keras.regularizers import l2

#from keras.models import model_from_json
from sklearn.model_selection import train_test_split


###### Traverse through folders to get images
tr = []
rootdir = r"D:\Education\TitaniumComics\2019\Images\Image Classification\Model\tensorflow-for-poets-2\tf_files\comics"
for subdir, dirs, files in os.walk(rootdir):
    for file in files:
        dic = {"filename": subdir.replace("\\", "/") + "/" + file,
               "label": subdir.split("\\")[-1]}
        tr.append(dic)

train_image = [] 
y = []
for i in tr:
    if i["filename"].endswith(".jpg"):
        img = image.load_img(i["filename"], target_size = (128, 128), grayscale = False)
        img = image.img_to_array(img)
        img = img/255
        train_image.append(img)
        y.append(i["label"])

X = np.array(train_image)

### one-hot encode the target variable.
labs = {i: index for index, i in enumerate(list(set(y)))}
import pandas as pd
lab_df = pd.DataFrame()
lab_df["names"] = list(set(y))
lab_df["labels"] = lab_df.index
lab_df.to_csv("D:/Education/TitaniumComics/2019/Images/Image Classification/FlaskApplication/labels.csv", index = False)

labels = [labs[item] for item in y]
labels = to_categorical(labels)

### Plot Images
import matplotlib.pyplot as plt
plt.figure()
plt.imshow(X[1])

X_train, X_test, y_train, y_test = train_test_split(X, labels, random_state=42, test_size=0.2)

############## Define Model Structure
input_img = Input(shape = (128, 128, 3))
tower_1 = Conv2D(128, (1,1), padding='same',kernel_regularizer=l2(0.01), bias_regularizer=l2(0.01), activation='relu')(input_img)
tower_1 = Conv2D(64, (3,3), padding='same', activation='relu')(tower_1)
tower_1 = layers.Dropout(0.8)(tower_1)

tower_2 = Conv2D(64, (1,1), kernel_regularizer=l2(0.01), bias_regularizer=l2(0.01), padding='same', activation='relu')(input_img)
tower_2 = Conv2D(32, (5,5), padding='same', activation='relu')(tower_2)
tower_2 = layers.Dropout(0.8)(tower_2)
tower_3 = MaxPooling2D((3,3), strides=(1,1), padding='same')(input_img)

tower_3 = Conv2D(64, (1,1), kernel_regularizer=l2(0.01), bias_regularizer=l2(0.01), padding='same', activation='relu')(tower_3)
tower_3 = layers.Dropout(0.8)(tower_3)

output = concatenate([tower_1, tower_2, tower_3], axis = 3)

output = Flatten()(output)
out    = Dense(10, activation='softmax')(output)

model = Model(inputs = input_img, outputs = out)
print(model.summary())

### Compile and fit model with SGD optimizer
epochs = 50
lrate = 0.01
decay = lrate/epochs
sgd = SGD(lr=lrate, momentum=0.9, decay=decay, nesterov=False)
model.compile(loss='categorical_crossentropy', optimizer=sgd, metrics=['accuracy'])
model.fit(X_train, y_train, validation_data=(X_test, y_test), epochs=epochs, batch_size=256)

## Model Accuracy
scores = model.evaluate(X_test, y_test, verbose=0)
print("Accuracy: %.2f%%" % (scores[1]*100))

#### Save Model
model_json = model.to_json()
with open("D:/model.json", "w") as json_file:
    json_file.write(model_json)
model.save_weights(os.path.join(os.getcwd(), 'model.h5'))